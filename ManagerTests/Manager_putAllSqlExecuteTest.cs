﻿using FluentAssertions;
using FluentAssertions.Execution;
using Manager.DataBaseHandling;
using Manager.DataBaseHandling.DataBaseHandling;
using Manager.Request;
using Manager.Result;
using NUnit.Framework;
using System.Linq;

namespace ka_tuwsbmanager_testProject
{
    class Manager_putAllSqlExecuteTest
    {
        [Test]
        public void ExecuteAllOnTest()
        {
            Manager_putAllRequest conditionPutAllRequest = new Manager_putAllRequest()
            {
                SetAllObjectsState = 1
            };

            Manager_putAllSqlExecute manager_putSqlExecute = new Manager_putAllSqlExecute();
            InformationalResult result = manager_putSqlExecute.Execute(conditionPutAllRequest);

            // Sprawdzenie stanu po włączeniu
            Manager_getAllRequest conditionGetAllRequest = new Manager_getAllRequest();
            Manager_getAllSqlExecute manager_GetAllSqlExecute = new Manager_getAllSqlExecute();
            ManagerGetAllInfoResult getAllResult = manager_GetAllSqlExecute.Execute(conditionGetAllRequest);

            using (new AssertionScope())
            {
                manager_putSqlExecute.Should().NotBeNull();
                result.Should().NotBeNull();
                result.ResultInfo.Should().NotContain("0");

                getAllResult.Should().NotBeNull();
                getAllResult.ObjectsList.Should().NotBeNull();
                getAllResult.ObjectsList.All(val => val.State == 1).Should().BeTrue();
            }
        }

        [Test]
        public void ExecuteAllOffTest()
        {
            Manager_putAllRequest conditionPutAllRequest = new Manager_putAllRequest()
            {
                SetAllObjectsState = 0
            };

            Manager_putAllSqlExecute manager_putSqlExecute = new Manager_putAllSqlExecute();
            InformationalResult result = manager_putSqlExecute.Execute(conditionPutAllRequest);

            // Sprawdzenie stanu po włączeniu
            Manager_getAllRequest conditionGetAllRequest = new Manager_getAllRequest();
            Manager_getAllSqlExecute manager_GetAllSqlExecute = new Manager_getAllSqlExecute();
            ManagerGetAllInfoResult getAllResult = manager_GetAllSqlExecute.Execute(conditionGetAllRequest);

            using (new AssertionScope())
            {
                manager_putSqlExecute.Should().NotBeNull();
                result.Should().NotBeNull();
                result.ResultInfo.Should().NotContain("0");

                getAllResult.Should().NotBeNull();
                getAllResult.ObjectsList.Should().NotBeNull();
                getAllResult.ObjectsList.All(val => val.State == 0).Should().BeTrue();
            }
        }
    }
}