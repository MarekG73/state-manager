using FluentAssertions;
using FluentAssertions.Execution;
using Manager.Operations;
using Manager.Selectors;
using NUnit.Framework;

namespace ka_tuwsbmanager_testProject
{
    public class ConditionCheck_SelectorTest
    {
        [Test]
        public void Selector_GetOperationNamesNotNullTest([Values("PZU_Reneval", "PZU_IOR", "DOOPA", "")] string p_value)
        {
            OperationSelector conditionCheckSelector = new OperationSelector("Operations");

            IOperation operation = conditionCheckSelector.SelectOperation(p_value);

            Assert.IsNotNull(operation);
        }

        [Test]
        public void Selector_GetOperationNamesNullOperationTest()
        {
            OperationSelector conditionCheckSelector = new OperationSelector("Operations");

            IOperation operation = conditionCheckSelector.SelectOperation("DOOPA");

            Assert.IsInstanceOf<NullOperation>(operation);
        }

        [Test]
        public void Selector_GetOperationNamesNullOperationNamesListTest()
        {
            OperationSelector conditionCheckSelector = new OperationSelector("Operations");

            IOperation operation = conditionCheckSelector.SelectOperation("DOOPA");

            Assert.IsNotEmpty(((NullOperation)operation).OperList);
            TestContext.Out.Write(((NullOperation)operation).OperList);
        }
                
        [Test]
        public void Selector_GetOperationNames_ManagerSet()
        {
            OperationSelector conditionCheckSelector = new OperationSelector("Operations");

            IOperation operation = conditionCheckSelector.SelectOperation("Manager_SetOperation");

            using(new AssertionScope())
            {
                operation.Should().NotBeNull();
                operation.Should().BeOfType(typeof(Manager_putOperation));
            }
        }

        [Test]
        public void Selector_GetOperationNames_ManagerCheck()
        {
            OperationSelector conditionCheckSelector = new OperationSelector("Operations");

            IOperation operation = conditionCheckSelector.SelectOperation("Manager_CheckOperation");

            using (new AssertionScope())
            {
                operation.Should().NotBeNull();
                operation.Should().BeOfType(typeof(Manager_getOperation));
            }
        }

    }
}