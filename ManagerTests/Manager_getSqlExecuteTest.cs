﻿using FluentAssertions;
using FluentAssertions.Execution;
using Manager.DataBaseHandling.DataBaseHandling;
using Manager.Request;
using Manager.Result;
using NUnit.Framework;

namespace ka_tuwsbmanager_testProject
{
    class Manager_getSqlExecuteTest
    {
        [Test]
        public void ExecuteTest()
        {
            Manager_getRequest conditionCheckRequest = new Manager_getRequest()
            {
                RequestObjectName = "PZU_PolicyBroker"
            };

            Manager_getSqlExecute manager_GetSqlExecute = new Manager_getSqlExecute();

            ManagerStateResult result = manager_GetSqlExecute.Execute(conditionCheckRequest);

            using (new AssertionScope())
            {
                manager_GetSqlExecute.Should().NotBeNull();
                result.Should().NotBeNull();
                result.ObjectData.State.Should().Be(0);
            }
        }
    }
}
