﻿using FluentAssertions;
using FluentAssertions.Execution;
using Manager.DataBaseHandling;
using Manager.DataBaseHandling.DataBaseHandling;
using Manager.Request;
using Manager.Result;
using NUnit.Framework;

namespace ka_tuwsbmanager_testProject
{
    class Manager_putSqlExecuteTest
    {
        [Test]
        public void ExecuteTest()
        {
            Manager_putRequest conditionCheckRequest = new Manager_putRequest()
            {
                PutRequestObjectData = new Manager.DbModel.StateManager()
                {
                    Name = "PZU_PolicyBroker",
                    GroupName = "PZU",
                    State = 1,
                    StateInfo = "Test dodawania / modyfikacji wpisu"
                }
            };

            Manager_putSqlExecute manager_putSqlExecute = new Manager_putSqlExecute();

            InformationalResult result = manager_putSqlExecute.Execute(conditionCheckRequest);

            using (new AssertionScope())
            {
                manager_putSqlExecute.Should().NotBeNull();
                result.Should().NotBeNull();
                result.ResultInfo.Should().Contain("1");
            }
        }
    }
}
