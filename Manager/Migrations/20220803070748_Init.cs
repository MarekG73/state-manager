﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Manager.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StateManager",
                columns: table => new
                {
                    idStateManager = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    GroupName = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    State = table.Column<byte>(type: "tinyint", nullable: false, defaultValueSql: "'1'"),
                    StateInfo = table.Column<string>(type: "TEXT", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.idStateManager);
                });

            migrationBuilder.CreateTable(
                name: "StateManagerUser",
                columns: table => new
                {
                    idStateManagerUser = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Username = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    Password = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.idStateManagerUser);
                });

            migrationBuilder.CreateIndex(
                name: "idStateManager_UNIQUE",
                table: "StateManager",
                column: "idStateManager",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Name_UNIQUE",
                table: "StateManager",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "idStateManagerUser_UNIQUE",
                table: "StateManagerUser",
                column: "idStateManagerUser",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Username_UNIQUE",
                table: "StateManagerUser",
                column: "Username",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StateManager");

            migrationBuilder.DropTable(
                name: "StateManagerUser");
        }
    }
}
