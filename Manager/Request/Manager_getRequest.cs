﻿using System.ComponentModel.DataAnnotations;

namespace Manager.Request
{
    /// <summary>
    /// Obiekt request do sprawdzenia stanu obiektu
    /// </summary>
    public class Manager_getRequest : IRequest
    {
        /// <summary>
        /// Nazwa obiektu
        /// </summary>
        [Required]
        public string RequestObjectName { get; set; }
    }
}
