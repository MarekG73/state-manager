﻿namespace Manager.Request
{
    /// <summary>
    /// Klasa żądania dla pobrania kompletu informacji o obiektach. 
    /// </summary>
    public class Manager_getAllRequest : IRequest
    {
        /// <summary>
        /// Nazwa grupy, której obiekty mają być pobrane, pusta oznacza zwrot danych wszystkich
        /// </summary>
        public string GroupName { get; set; }
    }
}
