﻿using Manager.DbModel;

namespace Manager.Request
{
    /// <summary>
    /// Obiekt request do usuwania wpisu na temat obiektu
    /// </summary>
    public class Manager_deleteRequest : IRequest
    {
        /// <summary>
        /// Dane obiektu
        /// </summary>
        public StateManager DeleteObjectData { get; set; } = null;
    }
}
