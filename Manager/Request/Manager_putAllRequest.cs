﻿namespace Manager.Request
{
    public class Manager_putAllRequest : IRequest
    {
        public byte SetAllObjectsState { get; set; }
    }
}
