﻿using Manager.DbModel;

namespace Manager.Request
{
    /// <summary>
    /// Obiekt request do ustawiania stanu obiektu
    /// </summary>
    public class Manager_putRequest : IRequest
    {
        /// <summary>
        /// Dane serwisu
        /// </summary>
        public StateManager PutRequestObjectData { get; set; } = null;
    }
}
