﻿using Manager.DbModel;

namespace Manager.Result
{
    /// <summary>
    /// Klasa wyniku operacji sprawdzenia stanu jednego obiektu
    /// </summary>
    public class ManagerStateResult : IResult
    {
        /// <summary>
        /// Informacja o przebiegu operacji - obecnie (11.2021) wypełniana TYLKO w przypadku błędu
        /// </summary>
        public string ResultInfo { get; set; } = string.Empty;
        /// <summary>
        /// Dane serwisu
        /// </summary>
        public StateManager ObjectData { get; set; }
    }
}
