﻿using Manager.DbModel;
using System.Collections.Generic;

namespace Manager.Result
{
    /// <summary>
    /// Klasa wyniku dla listy danych obiektów
    /// </summary>
    public class ManagerGetAllInfoResult : IResult
    {
        /// <summary>
        /// Informacja o przebiegu operacji - obecnie (11.2021) wypełniana TYLKO w przypadku błędu
        /// </summary>
        public string ResultInfo { get; set; } = string.Empty;
        /// <summary>
        /// Dane obiektów
        /// </summary>
        public List<StateManager> ObjectsList { get; set; }
    }
}
