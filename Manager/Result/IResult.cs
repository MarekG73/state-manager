﻿namespace Manager.Result
{
    /// <summary>
    /// Interface bazowy dla klas wyników
    /// </summary>
    public interface IResult
    {
        /// <summary>
        /// Informacja o przebiegu operacji - obecnie (11.2021) wypełniana TYLKO w przypadku błędu
        /// </summary>
        public string ResultInfo { get; set; }
    }
}
