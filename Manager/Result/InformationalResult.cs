﻿namespace Manager.Result
{
    /// <summary>
    /// Klasa wyniku operacji
    /// </summary>
    public class InformationalResult : IResult
    {
        /// <summary>
        /// Informacja o przebiegu operacji - obecnie (11.2021) wypełniana TYLKO w przypadku błędu
        /// </summary>
        public string ResultInfo { get; set; } = string.Empty;
    }
}
