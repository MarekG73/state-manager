﻿using Manager.DataBaseHandling.DbContext;
using Manager.DbModel;
using Manager.Request;
using Manager.Result;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Manager.DataBaseHandling.DataBaseHandling
{
    /// <summary>
    /// Wykonanie sprawdzenia stanu serwisu o podanej nazwie
    /// </summary>
    public class Manager_getSqlExecute
    {
        /// <summary>
        /// Przygotowanie i wykonanie akcji wyszukania w bazie
        /// </summary>
        /// <param name="p_conditionCheckRequest">Obiekt z podaną nazwą serwisu do wyszukania</param>
        /// <returns>Informacje o serwisie dla podanej nazwy serwisu. Brak danych => 'ArgumentException'</returns>
        public ManagerStateResult Execute(
            Manager_getRequest p_conditionCheckRequest)
        {
            ManagerStateResult conditionCheckResult = new ManagerStateResult();

            string requestWhere = p_conditionCheckRequest.RequestObjectName;

            using (Manager_Context dbContext = new Manager_Context())
            {
                conditionCheckResult = Select_Where(dbContext, requestWhere);                
            }
                        
            return conditionCheckResult;
        }

        /// <summary>
        /// Wykonanie zapytania do bazy w oparciu o otrzymane parametry dla WHERE
        /// </summary>
        /// <remarks>Rzuca 'ArgumentException' jeśli brak danych dla podanej nazwy serwisu</remarks>
        /// <param name="p_dbContext">Kontekst w ramach którego wykonywane jest zapytanie</param>
        /// <param name="p_modelRequestWhere">Warunki dla WHERE</param>
        /// <returns>Znalezione w bazie info o podanym serwisie</returns>
        private ManagerStateResult Select_Where(
            Manager_Context p_dbContext,
            string p_modelRequestWhere)
        {
            ManagerStateResult result = new ManagerStateResult();

            List<StateManager> modelResultObjectList =
                p_dbContext.TserviceManagers
                .Where(b =>
                b.Name == p_modelRequestWhere).ToList();

            if (modelResultObjectList != null && modelResultObjectList.Count > 0)
            {
                result.ObjectData = modelResultObjectList.First();
            }
            else
            {
                throw new ArgumentException("Brak danych dla serwisu o podanej nazwie.");
            }
                        
            return result;
        }
    }
}