﻿using Manager.DbModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Manager.DataBaseHandling.DbModel
{
    [Table("StateManagerUser")]
    [Index(nameof(Username), Name = "Username_UNIQUE", IsUnique = true)]
    [Index(nameof(idStateManagerUser), Name = "idStateManagerUser_UNIQUE", IsUnique = true)]
    public class StateManagerUser : IModel
    {
        [Key]
        public int idStateManagerUser { get; set; }
        [Required]
        [StringLength(50)]
        public string Username { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 8)]
        public string Password { get; set; }
    }
}