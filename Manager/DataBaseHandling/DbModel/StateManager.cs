﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace Manager.DbModel
{
    [Table("StateManager")]
    [Index(nameof(Name), Name = "Name_UNIQUE", IsUnique = true)]
    [Index(nameof(idStateManager), Name = "idStateManager_UNIQUE", IsUnique = true)]
    public partial class StateManager : IModel
    {
        [Key]
        public int idStateManager { get; set; }
        [Required]
        [StringLength(50)]
        public string GroupName { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Column(TypeName = "tinyint")]
        public byte State { get; set; }
        [StringLength(500)]
        public string StateInfo { get; set; }
    }
}
