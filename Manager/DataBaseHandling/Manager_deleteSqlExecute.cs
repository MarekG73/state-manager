﻿using Manager.DataBaseHandling.DbContext;
using Manager.Request;
using Manager.Result;
using System;
using System.Linq;

namespace Manager.DataBaseHandling.DataBaseHandling
{
    /// <summary>
    /// Wykonanie usunięcia danych z bazy
    /// </summary>
    public class Manager_deleteSqlExecute
    {
        /// <summary>
        /// Przygotowanie i wykonanie akcji dodania / zmiany danych w bazie
        /// </summary>
        /// <param name="p_deleteRequest"></param>
        /// <returns>Informacja o ilości dodanych / zmienionych rekordów</returns>
        public InformationalResult Execute(
            Manager_deleteRequest p_deleteRequest)
        {
            InformationalResult conditionCheckResult = new InformationalResult();

            using (Manager_Context dbContext = new Manager_Context())
            {
                conditionCheckResult = UpdateInsertServiceInfo(dbContext, p_deleteRequest);
            }
                        
            return conditionCheckResult;
        }

        /// <summary>
        /// Wykonanie operacji
        /// </summary>
        /// <param name="p_dbContext">Kontekst bazy dla operacji</param>
        /// <param name="p_deleteRequest">Obiekt z danymi do dodania / zmiany</param>
        /// <returns>Ilość rekordów dodanych / zmienionych</returns>
        private InformationalResult UpdateInsertServiceInfo(
            Manager_Context p_dbContext,
            Manager_deleteRequest p_deleteRequest)
        {
            InformationalResult operationResult = new InformationalResult();

            p_dbContext.Remove(p_dbContext.TserviceManagers.Single(
                ent => ent.Name == p_deleteRequest.DeleteObjectData.Name &&
                ent.GroupName == p_deleteRequest.DeleteObjectData.GroupName));         

            int recordsChanged = p_dbContext.SaveChanges();

            operationResult.ResultInfo = Convert.ToString(recordsChanged);

            return operationResult;
        }
    }
}