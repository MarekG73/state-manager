﻿using Manager.DataBaseHandling.DbContext;
using Manager.Request;
using Manager.Result;
using System;
using System.Linq;

namespace Manager.DataBaseHandling.DataBaseHandling
{
    /// <summary>
    /// Obsługa zapytań dla pobrań grupowych
    /// </summary>
    public class Manager_getAllSqlExecute
    {
        /// <summary>
        /// Przygotowanie i wykonanie akcji w bazie
        /// </summary>
        /// <param name="p_manager_GetAllRequest">Może zawierać nazwę towarzystwa, jeśli pusty => dane wszystkich serwisów w bazie</param>
        /// <returns></returns>
        public ManagerGetAllInfoResult Execute(
            Manager_getAllRequest p_manager_GetAllRequest)
        {
            ManagerGetAllInfoResult modelResultObjectList = new ManagerGetAllInfoResult();
                        
            using (Manager_Context dbContext = new Manager_Context())
            {
                if (string.IsNullOrEmpty(p_manager_GetAllRequest.GroupName))
                {
                    modelResultObjectList = Select(dbContext);
                }
                else
                {
                    modelResultObjectList = Select(dbContext, p_manager_GetAllRequest);
                }

                if (modelResultObjectList != null && modelResultObjectList.ObjectsList.Count > 0)
                {
                    modelResultObjectList.ResultInfo = string.Empty;
                }
                else
                {
                    // W zasadzie dla 204 nie potrzeba info...
                    throw new ArgumentException("Brak danych dla obiektów");
                }
            }

            return modelResultObjectList;
        }

        /// <summary>
        /// Wykonanie zapytania do bazy w oparciu o otrzymane parametry dla WHERE
        /// </summary>
        /// <param name="p_dbContext">Kontekst w ramach którego wykonywane jest zapytanie</param>
        /// <returns></returns>
        private ManagerGetAllInfoResult Select(
            Manager_Context p_dbContext)
        {
            ManagerGetAllInfoResult result = new ManagerGetAllInfoResult
            {
                ObjectsList = p_dbContext.TserviceManagers.ToList()
            };

            return result;
        }

        /// <summary>
        /// Wykonanie zapytania do bazy w oparciu o otrzymane parametry dla WHERE - tu dla podanego towarzystwa
        /// </summary>
        /// <param name="p_dbContext">Kontekst w ramach którego wykonywane jest zapytanie</param>
        /// <param name="p_manager_GetAllRequest">Nazwa towarzystwa, którego serwisy mają być pobrane</param>
        /// <returns></returns>
        private ManagerGetAllInfoResult Select(
            Manager_Context p_dbContext, 
            Manager_getAllRequest p_manager_GetAllRequest)
        {
            ManagerGetAllInfoResult result = new ManagerGetAllInfoResult
            {
                ObjectsList =
                p_dbContext.TserviceManagers.
                Where(sm =>
                sm.GroupName == p_manager_GetAllRequest.GroupName).ToList()
            };

            return result;
        }
    }
}
