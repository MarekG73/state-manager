﻿using Manager.DataBaseHandling.DbModel;
using Manager.DbModel;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Manager.DataBaseHandling.DbContext
{
    /// <summary>
    /// Reprezentuje sesję z bazą danych
    /// </summary>
    public partial class Manager_Context : Microsoft.EntityFrameworkCore.DbContext
    {
        public DbSet<StateManager> StateManager { get; set; }
        public DbSet<StateManagerUser> StateManagerUser { get; set; }

        public Manager_Context()
        {
        }

        public Manager_Context(DbContextOptions<Manager_Context> options)
            : base(options)
        {
        }

        public virtual DbSet<StateManager> TserviceManagers { get; set; }
        public virtual DbSet<StateManagerUser> TserviceManagerUsers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite("DataSource=ManagerDatabase.db;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StateManager>(entity =>
            {
                entity.HasKey(e => e.idStateManager)
                    .HasName("PRIMARY");

                entity.Property(e => e.State).HasDefaultValueSql("'1'");
            });

            modelBuilder.Entity<StateManagerUser>(entity =>
            {
                entity.HasKey(e => e.idStateManagerUser)
                    .HasName("PRIMARY");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
