﻿using Manager.DataBaseHandling.DbContext;
using Manager.Request;
using Manager.Result;
using Manager.ValidityControl.Exceptions;
using System;

namespace Manager.DataBaseHandling
{
    public class Manager_putAllSqlExecute
    {
        /// <summary>
        /// Przygotowanie i wykonanie akcji dodania / zmiany danych w bazie dla wszystkich serwisów
        /// </summary>
        /// <param name="p_putAllRequest"></param>
        /// <returns>Informacja o ilości dodanych / zmienionych rekordów</returns>
        public InformationalResult Execute(Manager_putAllRequest p_putAllRequest)
        {
            InformationalResult conditionCheckResult = new InformationalResult();

            using (Manager_Context dbContext = new Manager_Context())
            {
                conditionCheckResult = UpdateInsertServiceInfo(dbContext, p_putAllRequest);
            }

            return conditionCheckResult;
        }

        /// <summary>
        /// Wykonanie operacji
        /// </summary>
        /// <param name="p_dbContext">Kontekst bazy dla operacji</param>
        /// <param name="p_putAllRequest">Obiekt z danymi do ustalenia tryby zmiany (on / off)</param>
        /// <returns>Ilość rekordów dodanych / zmienionych</returns>
        private InformationalResult UpdateInsertServiceInfo(
            Manager_Context p_dbContext,
            Manager_putAllRequest p_putAllRequest)
        {
            InformationalResult operationResult = new InformationalResult();
            
            foreach (var serviceInfo in p_dbContext.TserviceManagers)
            {
                serviceInfo.State = p_putAllRequest.SetAllObjectsState;
            }

            int recordsChanged = p_dbContext.SaveChanges();

            if (recordsChanged > 0)
            {
                operationResult.ResultInfo = Convert.ToString(recordsChanged);
            }

            return operationResult;
        }
    }
}
