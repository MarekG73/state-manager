﻿using Manager.DataBaseHandling.DbContext;
using Manager.DbModel;
using Manager.Request;
using Manager.Result;
using Manager.ValidityControl.Exceptions;
using System;
using System.Linq;

namespace Manager.DataBaseHandling.DataBaseHandling
{
    /// <summary>
    /// Wykonanie dodania / zmiany danych w bazie
    /// </summary>
    public class Manager_putSqlExecute
    {
        /// <summary>
        /// Przygotowanie i wykonanie akcji dodania / zmiany danych w bazie
        /// </summary>
        /// <param name="p_updateInsertRequest"></param>
        /// <returns>Informacja o ilości dodanych / zmienionych rekordów</returns>
        public InformationalResult Execute(
            Manager_putRequest p_updateInsertRequest)
        {
            InformationalResult conditionCheckResult = new InformationalResult();

            using (Manager_Context dbContext = new Manager_Context())
            {
                conditionCheckResult = UpdateInsertServiceInfo(dbContext, p_updateInsertRequest);
            }
                        
            return conditionCheckResult;
        }

        /// <summary>
        /// Wykonanie operacji
        /// </summary>
        /// <param name="p_dbContext">Kontekst bazy dla operacji</param>
        /// <param name="p_updateInsertRequest">Obiekt z danymi do dodania / zmiany</param>
        /// <returns>Ilość rekordów dodanych / zmienionych</returns>
        private InformationalResult UpdateInsertServiceInfo(
            Manager_Context p_dbContext,
            Manager_putRequest p_updateInsertRequest)
        {
            InformationalResult operationResult = new InformationalResult();
            StateManager modelResultObject =
                p_dbContext.TserviceManagers.FirstOrDefault(tsm =>
                tsm.GroupName == p_updateInsertRequest.PutRequestObjectData.GroupName &&
                tsm.Name == p_updateInsertRequest.PutRequestObjectData.Name);

            // UPDATE
            if (modelResultObject != null)
            {
                modelResultObject.State = p_updateInsertRequest.PutRequestObjectData.State;
                modelResultObject.StateInfo = p_updateInsertRequest.PutRequestObjectData.StateInfo;

            }
            else
            {
                // INSERT
                modelResultObject = new StateManager
                {
                    State = p_updateInsertRequest.PutRequestObjectData.State,
                    StateInfo = p_updateInsertRequest.PutRequestObjectData.StateInfo,
                    GroupName = p_updateInsertRequest.PutRequestObjectData.GroupName,
                    Name = p_updateInsertRequest.PutRequestObjectData.Name
                };

                p_dbContext.TserviceManagers.Add(modelResultObject);
            }

            int recordsChanged = p_dbContext.SaveChanges();

            if (recordsChanged > 0)
            {
                operationResult.ResultInfo = Convert.ToString(recordsChanged);
            }           
            
            return operationResult;
        }
    }
}