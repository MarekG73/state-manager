﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Manager.Authentication
{
    /// <summary>
    /// Usługa logująca użytkownika
    /// </summary>
    public class UserService : IUserService
    {
        // Do testów!
        private List<User> _users = new List<User>
        {
            new User { Username = "admin", Password = "admin" }
        };

        public async Task<User> Authenticate(string username, string password)
        {
            User user = await Task.Run(() => _users.SingleOrDefault(x => x.Username == username && x.Password == password));

            // Jeśli autoryzacja niepoprawna: null jest zwracany bo brak użytkownikia
            // Jesli autoryzacja poprawna: zwrot obiektu User
            return user;
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await Task.Run(() => _users);
        }
    }
}
