﻿using System.ComponentModel.DataAnnotations;

namespace Manager.Authentication
{
    public class AuthenticateModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
