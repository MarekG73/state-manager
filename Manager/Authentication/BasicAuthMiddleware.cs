﻿using Microsoft.AspNetCore.Http;
using System;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Manager.Authentication
{
    public class BasicAuthMiddleware
    {
        private readonly RequestDelegate _next;

        public BasicAuthMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IUserService userService)
        {
            try
            {
                var authHeader = AuthenticationHeaderValue.Parse(context.Request.Headers["Authorization"]);
                var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
                var credentials = Encoding.UTF8.GetString(credentialBytes).Split(':', 2);
                var username = credentials[0];
                var password = credentials[1];

                // Sprawdzenie poświadczeń i próba udzielenia dostępu do contextu, czyli do zabezpieczonych zasobów
                context.Items["User"] = await userService.Authenticate(username, password);
            }
            catch
            {
                // Brak akcji - użytkownik nie jest zalogowany, brak dostępu do zabezpieczonych zasobów
            }

            await _next(context);
        }
    }
}
