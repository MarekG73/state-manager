﻿using System.Text.Json.Serialization;

namespace Manager.Authentication
{
    public class User
    {
        public string Username { get; set; }

        [JsonIgnore]
        public string Password { get; set; }
    }
}
