﻿using Manager.ValidityControl.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Net;

namespace Manager.ValidityControl.ExceptionFilters
{
    /// <summary>
    /// Obsługa błędów rzucanych podczas działania serwisu
    /// </summary>
    [ExcludeFromCodeCoverage]
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ErrorExceptionFilter : ExceptionFilterAttribute
    {
        /// <summary>
        /// Wykonanie operacji przechwycenia rzuconych błędów i reakcja zależna od rodzaju błędu
        /// </summary>
        /// <remarks>Obsługuje wszystkie błędy poza 'BadRequest' - to w walidacji modelu</remarks>
        /// <param name="p_context"></param>
        public override void OnException(ExceptionContext p_context)
        {
            HttpStatusCode statusCode;
            
            if (p_context.Exception is NotImplementedException)
            {
                statusCode = HttpStatusCode.NotFound;
                p_context.Result = new JsonResult(new
                {
                    error = new[] { p_context.Exception.Message }
                });
            }
            else if (p_context.Exception is ArgumentException)
            {
                statusCode = HttpStatusCode.NoContent;

                p_context.Result = new ContentResult()
                {
                    Content = p_context.Exception.Message
                };
            }
            else if (p_context.Exception is NoUpdatedOrChangedException)
            {
                statusCode = HttpStatusCode.NotModified;

                p_context.Result = new JsonResult(new
                {
                    error = new[] { p_context.Exception.Message }
                });
            }
            else
            {
                statusCode = HttpStatusCode.InternalServerError;

                p_context.Result = new JsonResult(new
                {
                    error = new[] { p_context.Exception.Message },
                    stackTrace = p_context.Exception.StackTrace
                });
            }

            p_context.HttpContext.Response.ContentType = "text/plain";
            p_context.HttpContext.Response.StatusCode = (int)statusCode;
        }
    }
}
