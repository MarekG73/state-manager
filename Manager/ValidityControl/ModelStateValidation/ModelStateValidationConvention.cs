﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;
using System.Diagnostics.CodeAnalysis;

namespace Manager.ValidityControl.ModelStateValidation
{
    [ExcludeFromCodeCoverage]
    public class ModelStateValidationConvention : IApplicationModelConvention
    {
        public void Apply(ApplicationModel application)
        {
            for (int i = 0; i < application.Controllers.Count; i++)
            {
                application.Controllers[i].Filters.Add(new ModelStateValidateAttribute());
            }
        }
    }
}
