﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Manager.ValidityControl.Exceptions
{
    /// <summary>
    /// Klasa błędu w przypadku braku wyników INSERT / UPDATE
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class NoUpdatedOrChangedException : Exception
    {
        public NoUpdatedOrChangedException()
        {
        }

        public NoUpdatedOrChangedException(string p_message)
            : base(p_message)
        {
        }

        public NoUpdatedOrChangedException(string p_message, Exception p_inner)
            : base(p_message, p_inner)
        {
        }
    }
}
