﻿using Manager.Contractors;
using Manager.Request;
using Manager.Result;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics.CodeAnalysis;

namespace Manager.Controllers
{
    /// <summary>
    /// Kontroler operacji managera 
    /// </summary>
    [ApiController]
    //[AllowAnonymous]
    //[Authorize]
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class StateManagerController : ControllerBase
    {
        /// <summary>
        /// Pobranie informacji na temat stanu obiektu (wraz z opisem na wypadek jeśli wyłączony)  
        /// </summary>
        /// <returns>Info o stanie obiektu z treścią komunikatu jeśli wyłączony</returns>
        [MapToApiVersion("1.0")]
        [MapToApiVersion("2.0")]
        [Route("[action]")]
        [ExcludeFromCodeCoverage]
        [HttpPost]        
        [Consumes("application/json")]
        [ProducesResponseType(typeof(ManagerStateResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
        public IActionResult CheckState([FromBody] Manager_getRequest p_managerCheckStateRequest)
        {
            OperationContractor conditionCheckContractor = new OperationContractor();
            ManagerStateResult checkResult =
                (ManagerStateResult)conditionCheckContractor
                .PrepareRunOperation(p_managerCheckStateRequest, "Manager_getOperation");

            return Ok(checkResult); 
        }

        /// <summary>
        /// Pobranie kompletu informacji na temat wpisów stanów obiektów
        /// </summary>
        /// <param name="p_managerGetAllRequest">Podanie w parametrze nazwy grupy daje w wyniku zwrot tylko dla tej grupy.
        ///  Pusty parametr pozwala zwrócić dane wszystkich obiektów</param>
        /// <returns>Info o stanie obiektu z treścią komunikatu jeśli wyłączony</returns>
        [MapToApiVersion("1.0")]
        [Route("[action]")]
        [ExcludeFromCodeCoverage]
        [HttpPost]        
        [Consumes("application/json")]
        [ProducesResponseType(typeof(ManagerGetAllInfoResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ContentResult), StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
        public ObjectResult GetAllInfo([FromBody] Manager_getAllRequest p_managerGetAllRequest)
        {
            OperationContractor conditionCheckContractor = new OperationContractor();
            ManagerGetAllInfoResult checkResult =
                (ManagerGetAllInfoResult)conditionCheckContractor
                .PrepareRunOperation(p_managerGetAllRequest, "Manager_getAllOperation");

            return Ok(checkResult);
        }

        /// <summary>
        /// Aktualizacja istniejącego wpisu (lub dodanie nowego)
        /// </summary>
        /// <param name="p_managerSetStateRequest">Dane obiektu i jego stanu</param>
        /// <returns>Kod statusu i ilość zaktualizowanych rekordów</returns>
        [MapToApiVersion("1.0")]
        [Route("[action]")]
        [ExcludeFromCodeCoverage]
        [HttpPost]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(InformationalResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status304NotModified, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
        public ObjectResult Update([FromBody] Manager_putRequest p_managerSetStateRequest)
        {
            OperationContractor conditionCheckContractor = new OperationContractor();
            InformationalResult checkResult =
                (InformationalResult)conditionCheckContractor
                .PrepareRunOperation(p_managerSetStateRequest, "Manager_putOperation");

            return Ok(checkResult);
        }

        /// <summary>
        /// Ustawienie stanu wszystkich obiektów
        /// </summary>
        /// <param name="p_managerSetAllStateRequest">Obiekt z informacją o ustawianym stanie</param>
        /// <returns>Kod statusu i ilość zaktualizowanych rekordów</returns>
        [MapToApiVersion("1.0")]
        [Route("[action]")]
        [ExcludeFromCodeCoverage]
        [HttpPost]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(InformationalResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status304NotModified, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
        public ObjectResult UpdateAll([FromBody] Manager_putAllRequest p_managerSetAllStateRequest)
        {
            OperationContractor conditionCheckContractor = new OperationContractor();
            InformationalResult checkResult =
                (InformationalResult)conditionCheckContractor
                .PrepareRunOperation(p_managerSetAllStateRequest, "Manager_putAllOperation");

            return Ok(checkResult);
        }

        /// <summary>
        /// Usunięcie istniejącego wpisu
        /// </summary>
        /// <param name="p_managerDeleteRequest">Dane obiektu i jego stanu</param>
        /// <returns>Kod statusu i ilość zaktualizowanych rekordów</returns>
        [MapToApiVersion("1.0")]
        [Route("[action]")]
        [ExcludeFromCodeCoverage]
        [HttpPost]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(InformationalResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status304NotModified, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
        public ObjectResult Delete([FromBody] Manager_deleteRequest p_managerDeleteRequest)
        {
            OperationContractor conditionCheckContractor = new OperationContractor();
            InformationalResult checkResult =
                (InformationalResult)conditionCheckContractor
                .PrepareRunOperation(p_managerDeleteRequest, "Manager_deleteOperation");

            return Ok(checkResult);
        }
    }
}
