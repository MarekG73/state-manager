﻿using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Diagnostics.CodeAnalysis;

namespace Manager.SwaggerHelper
{
    /// <summary>
    /// Klasa pomocnicza dla konfiguracji Swaggera, głównie wersjonowania
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ConfigureSwaggerOptions
        : IConfigureNamedOptions<SwaggerGenOptions>
    {
        private readonly IApiVersionDescriptionProvider _provider;

        public ConfigureSwaggerOptions(
            IApiVersionDescriptionProvider p_provider)
        {
            this._provider = p_provider;
        }

        public void Configure(SwaggerGenOptions p_options)
        {
            // Dodanie dokumentu swaggera dla każdej znalezionej wersji
            foreach (ApiVersionDescription description in _provider.ApiVersionDescriptions)
            {
                p_options.SwaggerDoc(
                    description.GroupName,
                    CreateVersionInfo(description));
            }
        }

        public void Configure(string p_name, SwaggerGenOptions p_options)
        {
            Configure(p_options);
        }

        private OpenApiInfo CreateVersionInfo(
                ApiVersionDescription p_description)
        {
            GetTitleAndDescription();

            OpenApiInfo info = new OpenApiInfo()
            {
                Title = "State Manager API",
                Description = "",
                Version = p_description.ApiVersion.ToString(),
            };

            if (p_description.IsDeprecated)
            {
                info.Description += " Ta wersja API jest przestarzała.";
            }

            return info;
        }

        private OpenApiInfoDataStruct GetTitleAndDescription()
        {
            OpenApiInfoDataStruct openApiInfoDataStruct = new OpenApiInfoDataStruct
            {
                Description = "",
                Title = ""
            };

            return openApiInfoDataStruct;
        }
    }
}