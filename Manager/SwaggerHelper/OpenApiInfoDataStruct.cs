﻿namespace Manager.SwaggerHelper
{
    public struct OpenApiInfoDataStruct
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
