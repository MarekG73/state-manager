﻿using Manager.DataBaseHandling;
using Manager.Request;
using Manager.Result;

namespace Manager.Operations
{
    /// <summary>
    /// Obsługa włączenia / wyłączenia wszystkich serwisów
    /// </summary>
    public class Manager_putAllOperation : IOperation
    {
        /// <summary>
        /// Wykonanie operacji włączenia / wyłączenia wszystkich serwisów
        /// </summary>
        /// <param name="p_putAllRequest"></param>
        /// <returns></returns>
        public IResult PerfomOperation(IRequest p_putAllRequest)
        {
            Manager_putAllSqlExecute sqlQueryExecutor = new Manager_putAllSqlExecute();
            InformationalResult conditionCheckResult =
                sqlQueryExecutor.Execute((Manager_putAllRequest)p_putAllRequest);

            return conditionCheckResult;
        }
    }
}
