﻿using Manager.DataBaseHandling.DataBaseHandling;
using Manager.Request;
using Manager.Result;

namespace Manager.Operations
{
    /// <summary>
    /// Obsługa pobrania danych wielu serwisów
    /// </summary>
    public class Manager_getAllOperation : IOperation
    {
        /// <summary>
        /// Wykonanie operacji pobrania danych wielu serwisów
        /// </summary>
        /// <param name="p_conditionCheckRequest"></param>
        /// <returns></returns>
        public IResult PerfomOperation(IRequest p_conditionCheckRequest)
        {
            Manager_getAllSqlExecute sqlQueryExecutor = new Manager_getAllSqlExecute();
            ManagerGetAllInfoResult conditionCheckResult = 
                sqlQueryExecutor.Execute((Manager_getAllRequest)p_conditionCheckRequest);

            return conditionCheckResult;
        }
    }
}
