﻿using Manager.DataBaseHandling.DataBaseHandling;
using Manager.Request;
using Manager.Result;

namespace Manager.Operations
{
    /// <summary>
    /// Obsługa usuwania danych serwisu
    /// </summary>
    public class Manager_deleteOperation : IOperation
    {
        /// <summary>
        /// Wykonanie operacji usuwania danych (ustawień) podanego serwisu
        /// </summary>
        /// <param name="p_updateInsertRequest"></param>
        /// <returns></returns>
        public IResult PerfomOperation(IRequest p_updateInsertRequest)
        {
            Manager_deleteSqlExecute sqlQueryExecutor = new Manager_deleteSqlExecute();
            InformationalResult conditionCheckResult =
                sqlQueryExecutor.Execute((Manager_deleteRequest)p_updateInsertRequest);

            return conditionCheckResult;            
        }
    }
}