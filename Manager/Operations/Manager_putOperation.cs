﻿using Manager.DataBaseHandling.DataBaseHandling;
using Manager.Request;
using Manager.Result;

namespace Manager.Operations
{
    /// <summary>
    /// Obsługa dodawania / zmiany danych serwisu
    /// </summary>
    public class Manager_putOperation : IOperation
    {
        /// <summary>
        /// Wykonanie operacji dodania / zmiany danych (ustawień) podanego serwisu
        /// </summary>
        /// <param name="p_updateInsertRequest"></param>
        /// <returns></returns>
        public IResult PerfomOperation(IRequest p_updateInsertRequest)
        {
            Manager_putSqlExecute sqlQueryExecutor = new Manager_putSqlExecute();
            InformationalResult conditionCheckResult =
                sqlQueryExecutor.Execute((Manager_putRequest)p_updateInsertRequest);

            return conditionCheckResult;            
        }
    }
}