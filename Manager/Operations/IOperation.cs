﻿using Manager.Request;
using Manager.Result;

namespace Manager.Operations
{
    public interface IOperation
    {
        public IResult PerfomOperation(IRequest p_request);
    }
}