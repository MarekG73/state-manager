﻿using Manager.DataBaseHandling.DataBaseHandling;
using Manager.Request;
using Manager.Result;

namespace Manager.Operations
{
    /// <summary>
    /// Obsługa pobrania danych jednego serwisu
    /// </summary>
    public class Manager_getOperation : IOperation
    {
        /// <summary>
        /// Wykonanie operacji pobrania danych jednego, wskazanego serwisu
        /// </summary>
        /// <param name="p_conditionCheckRequest"></param>
        /// <returns></returns>
        public IResult PerfomOperation(IRequest p_conditionCheckRequest)
        {
            Manager_getSqlExecute sqlQueryExecutor = new Manager_getSqlExecute();
            ManagerStateResult conditionCheckResult =
                sqlQueryExecutor.Execute((Manager_getRequest)p_conditionCheckRequest);

            return conditionCheckResult;
        }
    }
}
