﻿using Manager.Request;
using Manager.Result;

namespace Manager.Operations
{
    /// <summary>
    /// Klasa pustej operacji, tworzona w przypadku braku operacji o podanej w parametrze nazwie
    /// </summary>
    public class NullOperation : IOperation
    {
        /// <summary>
        /// Lista dostępnych operacji
        /// </summary>
        public string OperList { get; private set; }

        /// <summary>
        /// Jedynie przypisuje listę operacji
        /// </summary>
        /// <param name="p_operList"></param>
        public NullOperation(string p_operList)
        {
            OperList = p_operList;
        }

        /// <summary>
        /// Wykonanie pustej operacji - rzuca wyjątek z informacją
        /// </summary>
        /// <param name="p_conditionCheckRequest"></param>
        /// <returns></returns>
        public IResult PerfomOperation(IRequest p_conditionCheckRequest)
        {
            throw new System.NotImplementedException(
                "Brak wskazanej operacji. " + 
                "Dostępne operacje: " + OperList);
        }
    }
}
