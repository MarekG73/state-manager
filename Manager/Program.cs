using Manager.Authentication;
using Manager.DataBaseHandling.DbContext;
using Manager.SwaggerHelper;
using Manager.ValidityControl.ExceptionFilters;
using Manager.ValidityControl.ModelStateValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

{
    IServiceCollection services = builder.Services;

    services.AddDbContext<Manager_Context>();

    services.AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.Converters.Add(new StringEnumConverter());
                    options.SerializerSettings.TypeNameHandling = Newtonsoft.Json.TypeNameHandling.None;
                    options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Include;
                    options.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.All;
                });

    services.AddControllers();

    services.AddApiVersioning(setup =>
    {
        setup.DefaultApiVersion = new ApiVersion(1, 0);
        setup.AssumeDefaultVersionWhenUnspecified = true;
        setup.ReportApiVersions = true;
    });

    services.AddVersionedApiExplorer(setup =>
    {
        setup.GroupNameFormat = "'v'VVV";
        setup.SubstituteApiVersionInUrl = true;
    });

    services.AddSwaggerGen(c =>
    {
        c.IncludeXmlComments(string.Format(@"{0}\Manager.xml", AppDomain.CurrentDomain.BaseDirectory));
        c.EnableAnnotations();
        c.SwaggerGeneratorOptions = new Swashbuckle.AspNetCore.SwaggerGen.SwaggerGeneratorOptions()
        {
            DescribeAllParametersInCamelCase = false
        };

    }).AddSwaggerGenNewtonsoftSupport();

    services.AddScoped<IUserService, UserService>();

    services.Configure<MvcOptions>(x => x.Conventions.Add(new ModelStateValidationConvention()));
    services.AddMvc(options => options.Filters.Add(typeof(ErrorExceptionFilter)));
    services.ConfigureOptions<ConfigureSwaggerOptions>();
    services.AddHttpContextAccessor();
}

WebApplication app = builder.Build();

{    
    IWebHostEnvironment env = builder.Environment;
    IApiVersionDescriptionProvider provider = app.Services.GetRequiredService<IApiVersionDescriptionProvider>();

    if (env.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
    }

    app.UseHttpsRedirection();

    app.UseRouting();

    //app.UseMiddleware<BasicAuthMiddleware>();

    app.UseEndpoints(endpoints =>
    {
        endpoints.MapControllers();
    });

    app.UseSwagger(c =>
    {
        c.PreSerializeFilters.Add((swagger, httpReq) =>
        {
            swagger.Servers = new List<OpenApiServer> { new OpenApiServer { Url = $"{httpReq.Scheme}://{httpReq.Host.Value}" } };
        });
    });
    app.UseApiVersioning();
    app.UseDirectoryBrowser();

    app.UseSwaggerUI(options =>
    {
        foreach (var description in provider.ApiVersionDescriptions)
        {
            options.SwaggerEndpoint(
                $"/swagger/{description.GroupName}/swagger.json",
                description.GroupName.ToLowerInvariant());
            options.EnableFilter();
            options.DefaultModelExpandDepth(0); // Hide Models section in swagger UI
            options.DefaultModelsExpandDepth(-1);
            options.EnableValidator("https://editor.swagger.io/ ");
        }
    });

    app.UseHttpsRedirection();
}

app.Run();