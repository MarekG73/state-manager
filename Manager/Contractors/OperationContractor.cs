﻿using Manager.Operations;
using Manager.Request;
using Manager.Result;
using Manager.Selectors;

namespace Manager.Contractors
{
    /// <summary>
    /// Klasa zarządzająca wyborem operacji i wykonaniem przypisanej do niej akcji
    /// </summary>
    public class OperationContractor
    {
        /// <summary>
        /// Wybór operacji i wykonanie jej specyficznej akcji
        /// </summary>
        public IResult PrepareRunOperation(IRequest p_conditionCheckRequest, string p_operationName)
        {
            OperationSelector conditionCheckOperationSelector = 
                new OperationSelector("Operations");
            IOperation operation = conditionCheckOperationSelector.SelectOperation(p_operationName);

            return operation.PerfomOperation(p_conditionCheckRequest);
        }
    }
}
