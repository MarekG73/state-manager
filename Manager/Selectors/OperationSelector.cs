﻿using Manager.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Manager.Selectors
{
    /// <summary>
    /// Klasa wyboru operacji
    /// </summary>
    public class OperationSelector
    {
        /// <summary>
        /// Dostępne operacje
        /// </summary>
        private Type[] OperationsTypes { get; set; }

        /// <summary>
        /// Lista nazw dostępnych operacji
        /// </summary>
        public List<string> OperationsNamesList { get; private set; }

        /// <summary>
        /// Wybór (i sprawdzenie dostępności) operacji do wykonania
        /// </summary>
        /// <param name="p_operationsNamespace">Nazwa namespace zawierającej klasy operacji. 
        /// Uwaga - tylko namespace, bez nadrzędnego Manager!</param>
        public OperationSelector(string p_operationsNamespace)
        {
            OperationsTypes = GetTypesInNamespace("Manager", "Manager." + p_operationsNamespace);
            OperationsNamesList = GetTypesNamesList();
        }

        /// <summary>
        /// Wybór operacji. Nazwa musi odpowiadać nazwie z listy dozwolonych operacji!
        /// </summary>
        /// <param name="p_operationName">Nazwa operacji</param>
        /// <returns></returns>
        public IOperation SelectOperation(string p_operationName)
        {
            Type operation = OperationsTypes.FirstOrDefault(t => t.Name == p_operationName);
            
            if (operation == null)
            {
                return new NullOperation(GetTypesNamesInfoText());
            }

            Type constructed = operation.GetTypeInfo();
            object operationType = Activator.CreateInstance(constructed);

            return (IOperation)operationType;
        }

        /// <summary>
        /// Tekst z wykazem dostępnych operacji, na potrzeby zwrócenia błędu
        /// </summary>
        /// <returns></returns>
        private string GetTypesNamesInfoText()
        {
            string typesNames = "";

            for (int i = 0; i < OperationsNamesList.Count; i++)
            {
                if (OperationsNamesList[i] == "NullOperation" || OperationsNamesList[i] == "IOperation")
                {
                    continue;
                }

                typesNames += OperationsNamesList[i];
                typesNames += i + 1 == OperationsNamesList.Count ? "" : ", ";
            }

            return typesNames;
        }

        /// <summary>
        /// Wyodrębnienie nazw operacji dostępnych do wykonania
        /// </summary>
        /// <returns>Lista wyłacznie wywoływalnych operacji</returns>
        private List<string> GetTypesNamesList()
        {
            List<string> typesNamesList = new List<string>();
            
            for (int i = 0; i < OperationsTypes.Length; i++)
            {
                Type type = OperationsTypes[i];

                if (type.Name == "NullOperation" || type.Name == "IOperation")
                {
                    continue;
                }

                typesNamesList.Add(type.Name);
            }

            return typesNamesList;
        }

        /// <summary>
        /// Wyszukanie typów operacji w podanym namespace
        /// </summary>
        /// <param name="p_assemblyName">Nazwa projektu</param>
        /// <param name="p_namespace">Nazwa namespace w projekcie</param>
        /// <returns>Tablica WSZYSTKICH klas w danym namespace</returns>
        private Type[] GetTypesInNamespace(string p_assemblyName, string p_namespace)
        {
            Assembly assembly = Assembly.Load(p_assemblyName);
            return assembly.GetTypes()
                      .Where(t => string.Equals(
                          t.Namespace, p_namespace, StringComparison.Ordinal))
                      .ToArray();
        }
    }
}
